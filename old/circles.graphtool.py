from graph_tool.all import Graph, label_components, GraphView, dfs_search, all_circuits
import sys
from scipy.cluster.hierarchy import DisjointSet

min_circle_length = 150 # in bases
max_circle_nodes = 30 # number of nodes

import numpy as np


def load_graph_and_find_components(fasta_filename):
    nb_nodes = 0
    with open(fasta_filename, 'r') as file:
        for line in file:
            if line[0] == '>':
                nb_nodes += 1
    sys.stderr.write("First FASTA pass done\n")
    disjoint_set = DisjointSet()
    seqlen = 0
    seqlens  = np.zeros((nb_nodes,), dtype=np.uint64)
    node = None
    with open(fasta_filename, 'r') as file:
        for line in file:
            if line[0] == '>':
                if node is not None:
                    seqlens[node] = seqlen
                seqlen = 0
                ls = line.split()
                node = int(ls[0].split('_')[-1])
                disjoint_set.add(node)
                for elt in ls[1:]:
                    if elt[:2] == "L:":
                        node2 = int(elt.split(':')[2])
                        disjoint_set.add(node2)
                        disjoint_set.merge(node, node2)
            else:
                seqlen += len(line) - 1  # removes the \n
        # last node
        seqlens[node] = seqlen
    sys.stderr.write(f"Second FASTA pass done, {len(seqlens)} seqlens\n")

    # Postfilter: Remove components that are too small
    filtered_components = {}
    for subset in disjoint_set.subsets():
        total_length = kmer_size - 1 + sum([(seqlens[v] - (kmer_size - 1)) for v in subset])
        if total_length >= min_circle_length and len(subset) <= max_circle_nodes:
            filtered_components[next(iter(subset))] = subset

    nodes_to_load = set()
    for nodes in filtered_components.values():
        nodes_to_load |= set(nodes)
   
    print(len(filtered_components),"component found",len(nodes_to_load),"nodes to load")
    return nodes_to_load, filtered_components


def find_small_cycles(fasta_filename, nodes_to_load, component_to_nodes, kmer_size):
    circles = {}

    node_to_sequence = {}
    with open(fasta_filename, 'r') as file:
        current_node = None
        for line in file:
            if line[0] == '>':
                ls = line.split()
                current_node = int(ls[0].split('_')[-1])
                if current_node in nodes_to_load:
                    node_to_sequence[current_node] = ls

    for comp, nodes in component_to_nodes.items():
        subgraph = Graph(directed=False)
        subgraph.add_vertex(len(nodes))
        node_to_index = {n: i for i, n in enumerate(nodes)}
        for node in nodes:
            ls = node_to_sequence[node]
            for elt in ls:
                if elt[:2] == "L:":
                    node2 = int(elt.split(':')[2])
                    if node2 in nodes:
                        subgraph.add_edge(node_to_index[node], node_to_index[node2])

        if len(nodes) == 1:
            if subgraph.edge(0, 0) is not None:
                # print("Single-node component with self-loop found")
                pass
            else:
                continue
        elif len(nodes) == 2:
            if subgraph.edge(0, 1) is not None and subgraph.edge(1, 0) is not None:
                # print("Two-node component with cycle found")
                pass
            else:
                continue
        else:
            #if 100006 in nodes: print([x for x in all_circuits(subgraph)])
            print("FIXME: need a proper undirected icycle detection method for graph tool")
            #cycle_gen = all_circuits(subgraph) # this is wrong, because of undirected graph, it will always say there is a cycle if there are at least two nodes, see https://graph-tool.skewed.de/static/doc/autosummary/graph_tool.topology.all_circuits.html
            try:
                first_cycle = next(cycle_gen)
                # print("cycle found")
            except StopIteration:
                #continue
                pass

        for node in nodes:
            circles[node] = comp

    return circles

def output_circles(fasta_filename, vertices, circles_filename):
    g = open(circles_filename, "w")
    with open(fasta_filename, 'r') as file:
        output = False
        for line in file:
            if line[0] == '>':
                ls = line.split()
                node = int(ls[0].split('_')[-1])
                output = node in vertices
                if output:
                    ls[0] = ls[0] + "_circle_" + str(vertices[node])
                    g.write(" ".join(ls) +"\n")
            else:
                if output:
                    g.write(line)
    g.close()
 

def main(fasta_filename, kmer_size, circles_filename):
    # Load into a graph-tool graph
    ntl, ctn = load_graph_and_find_components(fasta_filename)

    # Detect small weakly connected components with cycles
    vertices = find_small_cycles(fasta_filename, ntl, ctn, kmer_size)
   
    output_circles(fasta_filename, vertices, circles_filename)
    
if __name__ == "__main__":
    import sys
    if len(sys.argv) < 4:
        print("Usage: python circles.graphtool.py <fasta_filename> <kmer_size> <circles_filename>")
    else:
        fasta_filename = sys.argv[1]
        kmer_size = int(sys.argv[2])
        circles_filename = sys.argv[3]
        main(fasta_filename, kmer_size, circles_filename)

