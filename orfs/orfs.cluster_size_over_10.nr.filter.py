#!/usr/bin/env python3

import sys

def main():
    """
    Reads DIAMOND tabular output from stdin (or a file) and:
      - Filters out alignments whose query coverage > 80%
      - Keeps track of counts of alignments with coverage >= 20%, >= 50%, >= 80%
      - Writes surviving lines (coverage <= 80%) to stdout
      - At the end, prints summary stats to stderr
    """
    count_cov_20 = 0
    count_cov_50 = 0
    count_cov_80 = 0
    total_lines = 0
    passed_filter = 0

    for line in sys.stdin:
        line = line.strip()
        if not line or line.startswith('#'):
            continue
        
        total_lines += 1
        cols = line.split('\t')

        # Expecting columns as:
        #  0      1       2       3       4        5       6      7     8       9      10      11     12
        # qseqid qstart qend   qlen   qstrand  sseqid sstart send slen  pident  evalue  cigar full_qseq
        qstart = int(cols[1])
        qend   = int(cols[2])
        qlen   = int(cols[3])

        # Compute coverage fraction
        aln_len = (qend - qstart + 1)
        coverage = aln_len / qlen

        # Tally how many alignments meet different coverage thresholds
        if coverage >= 0.2:
            count_cov_20 += 1
        if coverage >= 0.5:
            count_cov_50 += 1
        if coverage >= 0.8:
            count_cov_80 += 1

        # Filter out alignments if coverage > 0.8
        if coverage <= 0.8:
            passed_filter += 1
            print(line)

    # Print stats to stderr so as not to mix with filtered output
    sys.stderr.write(f"Total alignments read: {total_lines}\n")
    sys.stderr.write(f"Alignments with coverage >= 20%: {count_cov_20}\n")
    sys.stderr.write(f"Alignments with coverage >= 50%: {count_cov_50}\n")
    sys.stderr.write(f"Alignments with coverage >= 80%: {count_cov_80}\n")
    sys.stderr.write(f"Alignments passing (coverage <= 80%): {passed_filter}\n")

if __name__ == '__main__':
    main()

