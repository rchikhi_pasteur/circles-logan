#first run the .py
#then it outputs to .tsv
#then tsv to .txt
#then run this .sh
awk '
  NR == FNR {
    ids[$1] = 1;      # store IDs from orfs.cluster_size_over_10.nr.filter.txt
    next;
  }
  /^>/ {
    # Remove ">" from the start of the header
    header_line = substr($0, 2);
    # Split on space, the first piece is what we compare
    split(header_line, a, " ");
    header = a[1];
    # if this header is found in `ids`, skip it
    # otherwise, print
    if (header in ids) {
      keep = 0;
    } else {
      keep = 1; 
      print;
    }
    next;
  }
  keep { print; }
' orfs.cluster_size_over_10.nr.filter.txt orfs.cluster_size_over_10.fasta > orfs.cluster_size_over_10.nr.filter.fasta

