import fileinput
import os

def process_fasta(start_range, end_range, step):
    bins = {}
    start = start_range
    bin_ranges = []

    # Calculate bin ranges 
    print("Bins:")
    bins[f"below{start_range}"] = None
    print(f"0-{start_range}")
    while start < end_range:
        mean_length = start + step // 2
        lower_bound = int(start)
        upper_bound = int(mean_length * 1.1)
        
        bin_ranges.append((lower_bound, upper_bound))
        bin_name = f"{lower_bound}-{upper_bound}"
        print(bin_name)
        bins[bin_name] = None

        start = upper_bound * 0.9
        step *= 1.1

    bins[f"{upper_bound}plus"] = None

    output_dir = "binned_fasta"
    os.makedirs(output_dir, exist_ok=True)
    
    # Open file handles for each bin
    for bin_name in bins:
        bins[bin_name] = open(os.path.join(output_dir, f"bin_{bin_name}.fa"), 'w')

    with fileinput.input() as file:
        current_header = None
        current_sequence = []

        for line in file:
            line = line.strip()
            if line.startswith(">"):
                if current_header is not None:
                    write_sequence_to_bins(current_header, ''.join(current_sequence), bins, bin_ranges)
                current_header = line
                current_sequence = []
            else:
                current_sequence.append(line)
        
        # Write the last sequence
        if current_header is not None:
            write_sequence_to_bins(current_header, ''.join(current_sequence), bins, bin_ranges)
    
    # Close all file handles
    for bin_file in bins.values():
        bin_file.close()

def write_sequence_to_bins(header, sequence, bins, bin_ranges):
    length = len(sequence)
    binned = False
    if length < bin_ranges[0][0]:
        bins[f"below{bin_ranges[0][0]}"].write(f"{header}\n{sequence}\n")
        return
    for lower_bound, upper_bound in bin_ranges:
        #overlap = int(max(lower_bound, upper_bound) * 0.1)
        if lower_bound <= length < upper_bound:
            bins[f"{lower_bound}-{upper_bound}"].write(f"{header}\n{sequence}\n")
            binned = True
        #elif (lower_bound - overlap) <= length < (upper_bound + overlap):
        #    bins[f"{lower_bound}-{upper_bound}"].write(f"{header}\n{sequence}\n")
        #    binned = True
    if not binned:
        bins[f"{bin_ranges[-1][1]}plus"].write(f"{header}\n{sequence}\n")

if __name__ == "__main__":
    #input_file = "all_selfloops.fa.fixed" #now stdin
    start_range = 60
    end_range = 2000
    step = 40
    process_fasta(start_range, end_range, step)

