import networkx as nx
import sys
import unionfind

min_circle_length = 150 # in bases
max_circle_nodes = 30 # number of nodes

import numpy as np


def load_graph_and_find_components(fasta_filename):
    nb_nodes = 0
    with open(fasta_filename, 'r') as file:
        for line in file:
            if line[0] == '>':
                nb_nodes += 1
    sys.stderr.write(f"First FASTA pass done, {nb_nodes} nodes\n")
    #disjoint_set = DisjointSet()
    disjoint_set = unionfind.unionfind(nb_nodes)
    seqlen = 0
    seqlens = np.zeros((nb_nodes,), dtype=np.uint64)
    node = None
    with open(fasta_filename, 'r') as file:
        for line in file:
            if line[0] == '>':
                if node is not None:
                    seqlens[node] = seqlen
                seqlen = 0
                ls = line.split()
                node = int(ls[0].split('_')[-1])
                for elt in ls[1:]:
                    if elt[:2] == "L:":
                        node2 = int(elt.split(':')[2])
                        disjoint_set.unite(node, node2)
            else:
                seqlen += len(line) - 1  # removes the \n
        # last node
        seqlens[node] = seqlen
    ccs = disjoint_set.all_ccs() 
    sys.stderr.write(f"Second FASTA pass done, {len(ccs.keys())} connected components\n")

    # Postfilter: Remove components that are too small
    filtered_components = {}
    for cc, subset in disjoint_set.small_groups(ccs, max_circle_nodes):
        total_length = kmer_size - 1 + sum([(seqlens[v] - (kmer_size - 1)) for v in subset])
        if total_length >= min_circle_length: # and len(subset) <= max_circle_nodes:
            filtered_components[cc] = subset

    nodes_to_load = set()
    for nodes in filtered_components.values():
        nodes_to_load |= set(nodes)
   
    print(len(filtered_components),"small components",len(nodes_to_load),"nodes to load")
    return nodes_to_load, filtered_components


def find_small_cycles(fasta_filename, nodes_to_load, component_to_nodes, kmer_size):
    circles = {}

    node_to_header = {}
    with open(fasta_filename, 'r') as file:
        for line in file:
            if line[0] == '>':
                ls = line.split()
                current_node = int(ls[0].split('_')[-1])
                if current_node in nodes_to_load:
                    node_to_header[current_node] = ls[1:]

    nb_circles = 0
    for comp, nodes in component_to_nodes.items():
        subgraph = nx.Graph()
        node_to_index = {n: i for i, n in enumerate(nodes)}
        for node in nodes:
            ls = node_to_header[node]
            for elt in ls:
                if elt[:2] == "L:":
                    node2 = int(elt.split(':')[2])
                    if node2 in nodes:
                        subgraph.add_edge(node_to_index[node], node_to_index[node2])

        if len(nodes) == 1:
            if subgraph.has_edge(0, 0):
                # print("Single-node component with self-loop found")
                pass
            else:
                continue
        elif len(nodes) == 2:
            if (subgraph.has_edge(0, 1) and subgraph.has_edge(1, 0)) or subgraph.has_edge(0, 0) or subgraph.has_edge(1, 1):
                # print("Two-node component with cycle found")
                pass
            else:
                continue
        else:
            if any(nx.simple_cycles(subgraph)):
                pass
            else:
                continue

        nb_circles += 1
        for node in nodes:
            circles[node] = comp

    sys.stderr.write(f"Third FASTA pass done and cycles analyzed, {nb_circles} circles\n")

    return circles

def output_circles(fasta_filename, vertices, circles_filename):
    g = open(circles_filename, "w")
    with open(fasta_filename, 'r') as file:
        output = False
        for line in file:
            if line[0] == '>':
                ls = line.split()
                node = int(ls[0].split('_')[-1])
                output = node in vertices
                if output:
                    ls[0] = ls[0] + "_circle_" + str(vertices[node])
                    g.write(" ".join(ls) +"\n")
            else:
                if output:
                    g.write(line)
    g.close()
 

def main(fasta_filename, kmer_size, circles_filename):
    # Load into a graph-tool graph
    ntl, ctn = load_graph_and_find_components(fasta_filename)

    # Detect small weakly connected components with cycles
    vertices = find_small_cycles(fasta_filename, ntl, ctn, kmer_size)
   
    output_circles(fasta_filename, vertices, circles_filename)
    
if __name__ == "__main__":
    import sys
    if len(sys.argv) < 4:
        print("Usage: python circles.graphtool.py <fasta_filename> <kmer_size> <circles_filename>")
    else:
        fasta_filename = sys.argv[1]
        kmer_size = int(sys.argv[2])
        circles_filename = sys.argv[3]
        main(fasta_filename, kmer_size, circles_filename)

