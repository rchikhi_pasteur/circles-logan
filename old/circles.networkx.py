# this is older code, to demonstrate that networkx has bigger memory footprint

from convertToGFA import make_gfa

import networkx as nx

def load_gfa_to_networkx(gfa_filename):
    graph = nx.Graph()
    with open(gfa_filename, 'r') as file:
        for line in file:
            parts = line.strip().split('\t')
            if parts[0] == 'L':  # 'L' lines define links/edges in GFA
                from_node, to_node = int(parts[1]), int(parts[3])
                graph.add_edge(from_node, to_node)
    return graph

def detect_small_cycles(graph, max_size=30):
    # Function to detect small weakly connected components with a cycle
    #subgraphs = []
    circles = {}
    for ci, component in enumerate(nx.connected_components(graph)):
        subgraph = graph.subgraph(component)
        if len(subgraph) <= max_size :
            if any(nx.simple_cycles(subgraph)):
                #subgraphs.append(subgraph)
                for node in subgraph.nodes():
                    circles[node] = ci

    #return subgraphs
    return circles

def output_circles(fasta_filename, vertices, circles_filename):
    g = open(circles_filename, "w")
    with open(fasta_filename, 'r') as file:
        output = False
        for line in file:
            if line[0] == '>':
                ls = line.split()
                node = int(ls[0].split('_')[-1])
                output = node in vertices
                if output:
                    ls[0] = ls[0] + "_circle_" + str(vertices[node])
                    g.write(" ".join(ls) +"\n")
            else:
                if output:
                    g.write(line)
    g.close()
 
def main(fasta_filename, gfa_filename, kmer_size, circles_filename):
    # Convert FASTA to GFA
    make_gfa(fasta_filename, gfa_filename, kmer_size, False)
    
    # Load GFA into a NetworkX graph
    graph = load_gfa_to_networkx(gfa_filename)
    
    # Detect small weakly connected components with cycles
    vertices = detect_small_cycles(graph)
    
    # Output results
    #for i, cycle in enumerate(cycles):
    #    print(f"Cycle {i+1}: Nodes - {list(cycle.nodes())}")

    output_circles(fasta_filename, vertices, circles_filename)


if __name__ == "__main__":
    import sys
    if len(sys.argv) < 5:
        print("Usage: python circles.py <fasta_filename> <gfa_filename> <kmer_size> <circles_filename>")
    else:
        fasta_filename = sys.argv[1]
        gfa_filename = sys.argv[2]
        kmer_size = int(sys.argv[3])
        circles_filename = sys.argv[3]
        main(fasta_filename, gfa_filename, kmer_size, circles_filename)

